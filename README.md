# Логгер

Класс для логирования данных

## Документация

- **Подключение** 
    ```
    //подключаем скрипт логгера:
    include 'src/Logger.php'; 
    //установка пути до файла лога, если будем писать логи в файл:
    \Logger::$logPath = $_SERVER['DOCUMENT_ROOT'].'/log_path/log.log';
    ```
- **Запись лога в лог-файл**

    ```
    $testLogData = ['1'=>'test', 'key'=>'data'];
    
    // простая запись лога в лог-файл
    \Logger::log($testLogData);
  
    // подробная запись лога в лог-файл
    \Logger::log($testLogData, true);
  
    // запись трассировки в лог-файл
    \Logger::traceToLog();
    ```
  
- **Запись лога в консоль браузера**

    ```
    $testLogData = ['1'=>'test', 'key'=>'data'];
    
    // простая запись лога в консоль
    \Logger::console($testLogData);
    
    // простая запись лога в консоль с заголовком "testLogData"
    \Logger::console($testLogData, 'testLogData');
  
    // подробная запись лога в консоль
    \Logger::console($testLogData, null, true);
  
    // подробная запись лога в консоль с заголовком "testLogData"
    \Logger::console($testLogData, 'testLogData', true);
  
    // запись трассировки в консоль
    \Logger::traceToConsole();
  
    // запись трассировки в консоль с заголовком "trace"
    \Logger::traceToConsole('trace');
    ```
    
- **Запись лога в тег 'pre'**

    ```
    $testLogData = ['1'=>'test', 'key'=>'data'];
    
    // простая запись лога в тег 'pre'
    \Logger::pre($testLogData);
  
    // подробная запись лога в тег 'pre'
    \Logger::pre($testLogData, true);
  
    // запись трассировки в тег 'pre'
    \Logger::traceToPre(); 
    ```